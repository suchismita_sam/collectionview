//
//  ViewController.h
//  CollectionView
//
//  Created by Click Labs134 on 10/1/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

<UICollectionViewDataSource,UICollectionViewDelegate>
@end

