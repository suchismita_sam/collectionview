//
//  ViewController.m
//  CollectionView
//
//  Created by Click Labs134 on 10/1/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

NSArray *data;

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end



@implementation ViewController

@synthesize collectionView;

- (void)viewDidLoad {
    [super viewDidLoad];

//    data = [[NSMutableArray alloc] init];
//    for (int i=0; i<100; i++) {
//        [data addObject:[NSString stringWithFormat:@"suchismita", i]];
//    }
    
    [collectionView reloadData];
    // Do any additional setup after loading the view, typically from a nib.
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell1" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:4];
    label.text=@"Suchismita";
    
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}



-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}


-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
